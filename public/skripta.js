window.addEventListener('load', function() {
	//stran nalozena

	var prizgiCakanje = function() {
		document.querySelector(".loading").style.display = "block";
	};

	var ugasniCakanje = function() {
		document.querySelector(".loading").style.display = "none";
	};

	document.querySelector("#nalozi").addEventListener("click", prizgiCakanje);

	//Pridobi seznam datotek
	var pridobiSeznamDatotek = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				var datoteke = JSON.parse(xhttp.responseText);

				var datotekeHTML = document.querySelector("#datoteke");

				for (var i=0; i<datoteke.length; i++) {
					var datoteka = datoteke[i];

					var velikost = datoteka.velikost;
					
					var count = 0;
					
					while(velikost > 1024) {
						velikost /= 1024;
						count++;
					}
					
					var enota = "B";
					
					switch(count) {
						case 1:
							enota = "KiB";
							break;
						case 2:
							enota = "MiB";
							break;
						case 3:
							enota = "GiB";
							break;
						case 4:
							enota = "TiB";
							break;
						case 5:
							enota = "PiB";
							break;
						case 6:
							enota = "EiB";
							break;
						case 7:
							enota = "ZiB";
							break;
						case 8:
							enota = "YiB";
							break;
						default:
							for(var i = 0; i < count; i++) {
								velikost *= 1024;
							}
						
					}
					
					velikost = round(velikost);

					datotekeHTML.innerHTML += " \
						<div class='datoteka senca rob'> \
							<div class='naziv_datoteke'> " + datoteka.datoteka + "  (" + velikost + " " + enota + ") </div> \
							<div class='akcije'> \
							<span><a href='/poglej/" + datoteka.datoteka + "' target='_blank'>Poglej</a></span> \
							| <span><a href='/prenesi/" + datoteka.datoteka + "' target='_self'>Prenesi</a></span> \
							| <span akcija='brisi' datoteka='"+ datoteka.datoteka + "'>Izbriši</span> </div> \
					    </div>";
				}

				if (datoteke.length > 0) {
					var datZaBrisanje = document.querySelectorAll("span[akcija=brisi]");
					for(var i = 0; i < datZaBrisanje.length; i++) {
						var curr = datZaBrisanje[i];
						curr.addEventListener("click", brisi);
						curr.addEventListener("click", function() {
							curr.parentNode.parentNode.parentNode.removeChild(curr.parentNode.parentNode);
						});
					}
				}
				ugasniCakanje();
			}
		};
		xhttp.open("GET", "/datoteke", true);
		xhttp.send();
	};

	var brisi = function(event) {
		prizgiCakanje();
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				if (xhttp.responseText == "Datoteka izbrisana") {
					window.location = "/";
				} else {
					alert("Datoteke ni bilo možno izbrisati!");
				}
			}
			ugasniCakanje();
		};
		xhttp.open("GET", "/brisi/" + this.getAttribute("datoteka"), true);
		xhttp.send();
	};

	pridobiSeznamDatotek();

});

var round = function(num) {
	if((num % 1 )- 0.5 < 0) {
		return num - (num % 1);
	}
	else {
		return num - (num % 1) + 1;
	}
}